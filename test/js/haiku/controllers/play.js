angular.module('pl.paprikka.haiku.controllers.play', ['app.common.services.Modal', 'app.common.directives.CopyOnSelect']).controller('HaikuPlayCtrl', [
  '$location', '$routeParams', '$rootScope', '$scope', '$timeout', 'Remote', 'Slides', 'Modal', 'Notify', 'PageState', function($location, $routeParams, $rootScope, $scope, $timeout, Remote, Slides, Modal, Notify, PageState) {
    var sendCtrl, shareCtrl, showUI, _ref;
    PageState.enable();
    showUI = function() {
      return $scope.UIReady = true;
    };
    $timeout(showUI, 2000);
    $rootScope.$on('haiku:remote:URLShared', function() {
      return Notify.info('Invitations sent.');
    });
    $rootScope.$on('haiku:remote:URLSent', function() {
      return Notify.info('Remote bookmark sent.');
    });
    $rootScope.$on('haiku:room:remoteJoined', function() {
      console.log('Haiku::Remote joined, sending current status...');
      $scope.updateStatus(Slides["package"]($scope.categories));
      return Notify.info('Remote connected.');
    });
    $rootScope.$on('haiku:room:guestJoined', function() {
      return Notify.info('New guest has joined.');
    });
    $scope.updateStatus = function(data) {
      return Remote.sendUpdates(data, $routeParams.roomID);
    };
    sendCtrl = function($scope, $modalInstance) {
      $scope.result = {};
      $scope.ok = function() {
        return $modalInstance.close($scope.result.email);
      };
      return $scope.cancel = function() {
        return $modalInstance.dismiss('cancel');
      };
    };
    $scope.sendRemoteURL = function() {
      var modalInstance;
      modalInstance = Modal.open({
        templateUrl: 'haiku/partials/modals/send-remote-url.html',
        controller: sendCtrl
      });
      return modalInstance.result.then(function(email) {
        return Remote.sendRemoteURL($routeParams.roomID, email);
      });
    };
    shareCtrl = function($scope, $modalInstance) {
      var getCurrentURL;
      $scope.newEmail = {
        value: ''
      };
      $scope.emails = [];
      getCurrentURL = function() {
        return location.toString().replace(/#\/play\//i, "#/view/");
      };
      $scope.currentURL = getCurrentURL();
      $scope.add = function(email) {
        if (email != null ? email.length : void 0) {
          $scope.emails.push(email);
          return $scope.newEmail.value = '';
        }
      };
      $scope.remove = function(email) {
        return $scope.emails = _.filter($scope.emails, function(em) {
          return em !== email;
        });
      };
      $scope.ok = function() {
        return $modalInstance.close($scope.emails);
      };
      return $scope.cancel = function() {
        return $modalInstance.dismiss('cancel');
      };
    };
    $scope.shareURL = function() {
      var modalInstance;
      modalInstance = Modal.open({
        templateUrl: 'haiku/partials/modals/share.html',
        controller: shareCtrl
      });
      return modalInstance.result.then(function(emails) {
        console.log(emails);
        return Remote.shareURL($routeParams.roomID, emails);
      });
    };
    if (!((_ref = $rootScope.categories) != null ? _ref.length : void 0)) {
      $location.path('/');
    }
    return $scope.navVisible = true;
  }
]);
