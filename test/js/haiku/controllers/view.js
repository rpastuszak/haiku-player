angular.module('pl.paprikka.haiku.controllers.view', []).controller('HaikuViewCtrl', [
  '$location', '$routeParams', '$rootScope', '$scope', '$timeout', 'Remote', function($location, $routeParams, $rootScope, $scope, $timeout, Remote) {
    $scope.status = 'loading';
    $rootScope.clientRole = 'guest';
    $scope.test = 'bar';
    if (!$routeParams.roomID) {
      $location.path('/');
    }
    Remote.join($routeParams.roomID);
    return $rootScope.$on('haiku:room:joined', function(scope, data) {
      Remote.broadcastJoinedGuest(data.room);
      return $scope.$apply(function() {
        var showUI;
        $rootScope.categories = data.categories;
        $scope.status = 'ready';
        showUI = function() {
          return $scope.UIReady = true;
        };
        return $timeout(showUI, 2000);
      });
    });
  }
]);
