angular.module('pl.paprikka.haiku.controllers.import', ['pl.paprikka.haiku.services.importer', 'app.common.services.Notify']).controller('HaikuImportCtrl', [
  'Importer', '$location', '$rootScope', '$scope', 'Remote', 'Modal', 'Notify', function(Importer, $location, $rootScope, $scope, Remote, Modal, Notify) {
    $rootScope.categories = [];
    $scope.files = [];
    $scope.mdSupported = navigator.userAgent.match(/(iPad|iPhone|iPod)/g) ? false : true;
    $rootScope.$on('haiku:room:accepted', function(e, data) {
      $rootScope.clientRole = 'host';
      $location.path('/play/' + data.room);
      return $scope.$apply();
    });
    $scope.initConnection = function(categories) {
      return Remote.request(categories);
    };
    return $scope.onFileDropped = function(data) {
      return _.defer(function() {
        return $scope.$apply(function() {
          $rootScope.categories = Importer.getFromFiles(data);
          if ($rootScope.categories.length) {
            $scope.initConnection($rootScope.categories);
          }
          return console.log('Categories loaded: ', $scope.categories);
        });
      });
    };
  }
]);
