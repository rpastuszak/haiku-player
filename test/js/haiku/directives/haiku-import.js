angular.module('pl.paprikka.directives.haiku-import', ['pl.paprikka.haiku.services.slides', 'pl.paprikka.services.hammerjs', 'pl.paprikka.haiku.services.remote', 'pl.paprikka.directives.haiku.hTap', 'ngSanitize']).directive('haikuImport', [
  '$window', 'Slides', 'Hammer', 'Remote', '$rootScope', function($window, Slides, Hammer, Remote, $rootScope) {
    return {
      templateUrl: 'haiku/partials/haiku-import.html',
      restrict: 'AE',
      link: function(scope, elm, attrs) {}
    };
  }
]);
