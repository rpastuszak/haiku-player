angular.module('pl.paprikka.directives.haiku', []).directive('haiku', [
  '$window', 'Hammer', 'Slides', '$rootScope', function($window, Hammer, Slides, $rootScope) {
    return {
      scope: {
        categories: '=',
        onUpdate: '&'
      },
      templateUrl: 'haiku/partials/haiku.html',
      restrict: 'AE',
      link: function(scope, elm, attrs) {
        var initSettings, onKeyDown, onMouseWheel;
        initSettings = function(scope) {
          scope.categories.currentCategory = 0;
          scope.categories.currentSlide = 0;
          scope.isLastCategory = false;
          scope.isLastSlide = false;
          scope.isFirstCategory = false;
          return scope.isFirstSlide = false;
        };
        initSettings(scope);
        scope.$watch('categories.length', function(n, o) {
          if (n === o) {
            return;
          }
          return initSettings(scope);
        });
        scope.updatePosition = function() {
          var currCat, currSlide, _ref, _ref1;
          console.log('haiku::updatePosition');
          _.each(scope.categories, function(cat, catIndex) {
            if (catIndex < scope.categories.currentCategory) {
              cat.status = 'prev';
            } else if (catIndex === scope.categories.currentCategory) {
              cat.status = 'current';
            } else if (catIndex > scope.categories.currentCategory) {
              cat.status = 'next';
            }
            return _.each(cat.slides, function(slide) {
              if (slide.index < scope.categories.currentSlide) {
                return slide.status = 'prev';
              } else if (slide.index === scope.categories.currentSlide) {
                return slide.status = 'current';
              } else if (slide.index > scope.categories.currentSlide) {
                return slide.status = 'next';
              }
            });
          });
          currCat = scope.categories.currentCategory;
          currSlide = scope.categories.currentSlide;
          scope.isLastCategory = currCat === scope.categories.length - 1 ? true : false;
          scope.isLastSlide = currSlide === ((_ref = scope.categories[currCat]) != null ? (_ref1 = _ref.slides) != null ? _ref1.length : void 0 : void 0) - 1 ? true : false;
          scope.isFirstCategory = currCat === 0 ? true : false;
          scope.isFirstSlide = currSlide === 0 ? true : false;
          return typeof scope.onUpdate === "function" ? scope.onUpdate({
            status: Slides["package"](scope.categories)
          }) : void 0;
        };
        scope.prevCategory = function() {
          if (!scope.isFirstCategory) {
            scope.categories.currentCategory = scope.categories.currentCategory - 1;
            return scope.categories.currentSlide = 0;
          }
        };
        scope.nextCategory = function() {
          if (!scope.isLastCategory) {
            scope.categories.currentCategory = scope.categories.currentCategory + 1;
            return scope.categories.currentSlide = 0;
          }
        };
        scope.prevSlide = function() {
          if (!scope.isFirstSlide) {
            return scope.categories.currentSlide = scope.categories.currentSlide - 1;
          }
        };
        scope.nextSlide = function() {
          if (!scope.isLastSlide) {
            return scope.categories.currentSlide = scope.categories.currentSlide + 1;
          }
        };
        scope.$watch('categories.currentCategory', scope.updatePosition);
        scope.$watch('categories.currentSlide', scope.updatePosition);
        Hammer(elm).on('swipeleft', function(e) {
          e.gesture.srcEvent.preventDefault();
          scope.$apply(scope.nextCategory);
          return false;
        });
        Hammer(elm).on('swiperight', function(e) {
          e.gesture.srcEvent.preventDefault();
          scope.$apply(scope.prevCategory);
          return false;
        });
        Hammer(elm).on('swipeup', function(e) {
          e.gesture.srcEvent.preventDefault();
          scope.$apply(scope.nextSlide);
          return false;
        });
        Hammer(elm).on('swipedown', function(e) {
          e.gesture.srcEvent.preventDefault();
          scope.$apply(scope.prevSlide);
          return false;
        });
        onKeyDown = function(e) {
          if (!scope.$$phase) {
            return scope.$apply(function() {
              switch (e.keyCode) {
                case 37:
                  return scope.prevCategory();
                case 38:
                  return scope.prevSlide();
                case 39:
                  return scope.nextCategory();
                case 40:
                  return scope.nextSlide();
              }
            });
          }
        };
        onMouseWheel = function(e) {
          var delta, treshold;
          delta = e.originalEvent.wheelDelta;
          treshold = 100;
          return scope.$apply(function() {
            if (delta < -treshold) {
              scope.nextSlide();
            }
            if (delta > treshold) {
              return scope.prevSlide();
            }
          });
        };
        $($window).on('keydown', onKeyDown);
        $($window).on('mousewheel', onMouseWheel);
        $rootScope.$on('haiku:remote:control', function(e, data) {
          return scope.$apply(function() {
            if (data.command === 'position') {
              return scope.goto({
                index: data.params.currentSlide,
                categoryIndex: data.params.currentCategory
              });
            }
          });
        });
        scope.$on('haiku:goto', function(slide) {
          return scope.goto(slide);
        });
        scope.$on('haiku:goto', function(slide) {
          return scope.goto(slide);
        });
        $rootScope.$on('haiku:remote:goto', function(e, slide) {
          return scope.goto(slide);
        });
        scope.getCategoryClass = function(category) {
          return 'haiku__category--' + (category.status || 'prev');
        };
        scope.getSlideClass = function(slide) {
          return 'haiku__slide--' + (slide.status || 'prev');
        };
        scope.getSlideStyle = function(slide) {
          return {
            'background': slide.background || '#333',
            'background-size': 'cover'
          };
        };
        scope.isCurrentSlide = function(slide) {
          return Slides.isCurrentSlide(slide, categories);
        };
        scope.goto = function(slide) {
          scope.categories.currentCategory = slide.categoryIndex;
          return scope.categories.currentSlide = slide.index;
        };
        return scope.getThemeClass = function() {
          return 'haiku--default';
        };
      }
    };
  }
]);
