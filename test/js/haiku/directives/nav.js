angular.module('pl.paprikka.haiku.directives.nav', ['pl.paprikka.haiku.services.slides']).directive('haikuNav', [
  '$rootScope', '$location', 'Slides', 'Modal', function($rootScope, $location, Slides, Modal) {
    return {
      scope: {
        categories: '=',
        visible: '=',
        control: '&navControl',
        enableRemote: '&onEnableRemote',
        share: '&onShare'
      },
      restrict: 'AE',
      replace: true,
      templateUrl: 'haiku/partials/directives/nav.html',
      link: function(scope, elm, attr) {
        var closeCtrl;
        scope.clientRole = $rootScope.clientRole;
        scope.goto = function(slide) {
          return $rootScope.$emit('haiku:remote:goto', slide);
        };
        scope.isCurrentCategory = function(cat) {
          return Slides.isCurrentCategory(cat, scope.categories);
        };
        scope.isCurrentSlide = function(slide) {
          return Slides.isCurrentSlide(slide, scope.categories);
        };
        closeCtrl = function($scope, $modalInstance) {
          $scope.ok = function() {
            return $modalInstance.close(true);
          };
          return $scope.cancel = function() {
            return $modalInstance.dismiss('cancel');
          };
        };
        return scope.close = function() {
          var modalInstance;
          modalInstance = Modal.open({
            templateUrl: 'haiku/partials/modals/close.html',
            controller: closeCtrl
          });
          return modalInstance.result.then(function(result) {
            if (result) {
              return $location.path('/');
            }
          });
        };
      }
    };
  }
]);
