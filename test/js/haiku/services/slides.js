angular.module('pl.paprikka.haiku.services.slides', []).factory('Slides', [
  function() {
    var Slides;
    Slides = function() {};
    Slides.getCurrentCategory = function(categories) {
      return _.find(categories, {
        status: 'current'
      });
    };
    Slides.isCurrentCategory = function(cat) {
      return cat.status === 'current';
    };
    Slides.isCurrentSlide = function(slide, categories) {
      return slide.index === categories.currentSlide && slide.categoryIndex === categories.currentCategory;
    };
    Slides["package"] = function(categories) {
      var cleanedCategories;
      cleanedCategories = _.map(categories, function(cat) {
        return {
          slides: _.map(cat.slides, function(slide) {
            return {
              index: slide.index,
              categoryIndex: slide.categoryIndex
            };
          })
        };
      });
      cleanedCategories = JSON.parse(angular.toJson(cleanedCategories));
      return _.extend({
        categories: cleanedCategories
      }, _.pick(categories, 'currentCategory', 'currentSlide'));
    };
    return Slides;
  }
]);
