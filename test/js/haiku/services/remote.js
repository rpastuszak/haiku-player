angular.module('pl.paprikka.haiku.services.remote', ['app.common.webSockets']).service('Remote', [
  'WebSockets', '$rootScope', function(WebSockets, $rootScope) {
    var HUB_LOCATION, Remote, SOCKET_LOCATION, socket;
    console.log('Remote::init');
    HUB_LOCATION = 'http://haiku-hub.herokuapp.com:80';
    SOCKET_LOCATION = location.hostname.split('.')[0] === '192' ? location.hostname + ':8082' : HUB_LOCATION;
    socket = WebSockets.connect(SOCKET_LOCATION);
    socket.on('connect', function() {
      console.log('haiku::Remote::connected');
      socket.on('remote', function(data) {
        return $rootScope.$emit('haiku:remote:control', data);
      });
      socket.on('room:remoteJoined', function(data) {
        return $rootScope.$emit('haiku:room:remoteJoined', data);
      });
      socket.on('room:guestJoined', function(data) {
        return $rootScope.$emit('haiku:room:guestJoined', data);
      });
      socket.on('room:accepted', function(data) {
        return $rootScope.$emit('haiku:room:accepted', data);
      });
      socket.on('room:joined', function(data) {
        return $rootScope.$emit('haiku:room:joined', data);
      });
      socket.on('remote:URLSent', function(data) {
        return $rootScope.$emit('haiku:remote:URLSent', data);
      });
      return socket.on('remote:URLShared', function(data) {
        return $rootScope.$emit('haiku:remote:URLShared', data);
      });
    });
    return Remote = {
      join: function(room) {
        return socket.emit('room:join', {
          room: room
        });
      },
      leave: function(room, cb) {
        return socket.emit('room:leave', {
          room: room
        });
      },
      request: function(categories) {
        return socket.emit('room:request', {
          categories: categories
        });
      },
      sendRemoteURL: function(room, to) {
        return socket.emit('remote:sendURL', {
          room: room,
          to: to
        });
      },
      shareURL: function(room, to) {
        return socket.emit('remote:shareURL', {
          room: room,
          to: to
        });
      },
      broadcastJoinedGuest: function(room) {
        return socket.emit('remote:guestJoined', {
          room: room
        });
      },
      sendUpdates: function(data, room) {
        console.log('haiku::update', data, room, socket);
        return socket.emit('remote:update', {
          data: data,
          room: room
        });
      }
    };
  }
]);
