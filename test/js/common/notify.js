angular.module('app.common.services.Notify', []).factory('Notify', function() {
  toastr.options.positionClass = 'toast-bottom-right';
  toastr.options.hideDuration = 2;
  return window.toastr;
});
