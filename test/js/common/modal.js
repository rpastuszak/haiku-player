angular.module("app.common.services.Modal", ['ui.bootstrap', 'ui.bootstrap.tpls']).service('Modal', [
  '$modal', function($modal) {
    window.m = $modal;
    return $modal;
  }
]).service('ModalDefaults', function() {
  var settings;
  return settings = {
    buttons: {
      yesNo: [
        {
          label: 'No',
          result: false,
          cssClass: 'btn--default'
        }, {
          label: 'Yes',
          result: true,
          cssClass: 'btn--positive'
        }
      ]
    }
  };
});
