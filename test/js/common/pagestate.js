angular.module('app.common.services.pageState', []).service('PageState', [
  '$window', function($window) {
    var PageState;
    PageState = (function() {
      function PageState() {}

      PageState.prototype.enable = function() {
        if (location.hostname === 'localhost' || location.hostname.split('.')[0] === '192') {
          return;
        }
        return $window.onbeforeunload = function() {
          return 'Do you really want to leave and remove your haiku?';
        };
      };

      PageState.prototype.disable = function() {
        return $window.onbeforeunload = null;
      };

      return PageState;

    })();
    return new PageState;
  }
]);
