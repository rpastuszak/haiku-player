angular.module("app.common.services.Dialog", ['ui.bootstrap', 'ui.bootstrap.tpls']).service('Dialog', [
  '$dialog', function($dialog) {
    window.d = $dialog;
    return $dialog;
  }
]).service('DialogDefaults', function() {
  var settings;
  return settings = {
    buttons: {
      yesNo: [
        {
          label: 'No',
          result: false,
          cssClass: 'btn--default'
        }, {
          label: 'Yes',
          result: true,
          cssClass: 'btn--positive'
        }
      ]
    }
  };
});
