angular.module('app.common.directives.CopyOnSelect', []).directive('copyOnSelect', [
  function() {
    return {
      restrict: 'A',
      link: function(scope, elm, attr) {
        return elm.on('focus', function(e) {
          var _this = this;
          return _.defer(function() {
            return _this.select();
          });
        });
      }
    };
  }
]);
