angular.module('pl.paprikka.directives.drop', []).directive('ppkDrop', [
  '$window', function($window) {
    return {
      restrict: 'AE',
      templateUrl: 'drop/drop.html',
      replace: true,
      scope: {
        onDrop: '&',
        files: '='
      },
      link: function(scope, elm, attrs) {
        var boxEl, getFileType, getImageFiles, getTextFiles, onDragEnter, onDragLeave, onDragOver, onDrop;
        boxEl = elm.find('.ppk-drop__box')[0];
        scope.isFileOver = false;
        onDragEnter = function(e) {
          return scope.$apply(function() {
            return scope.isFileOver = true;
          });
        };
        onDragLeave = function(e) {
          return scope.$apply(function() {
            return scope.isFileOver = false;
          });
        };
        onDragOver = function(e) {
          return e.preventDefault();
        };
        onDrop = function(e) {
          var dt, type, _ref;
          e.stopPropagation();
          e.preventDefault();
          dt = e.dataTransfer;
          scope.$apply(function() {
            scope.files = dt.files;
            return scope.isFileOver = false;
          });
          if ((_ref = dt.files) != null ? _ref[0] : void 0) {
            type = getFileType(dt.files[0]);
            if (type === 'text') {
              getTextFiles(dt.files[0], scope.onDrop);
            }
            if (type === 'images') {
              return getImageFiles(dt.files, scope.onDrop);
            }
          }
        };
        getFileType = function(fileDesc) {
          var ext, regex, _ref;
          regex = /(\.[^.]+)$/i;
          ext = (_ref = fileDesc.name.match(regex)) != null ? _ref[0] : void 0;
          if (fileDesc.type === '') {
            switch (ext) {
              case '.md':
                return 'text';
              case '.txt':
                return 'text';
            }
          } else if (fileDesc.type.split('/')[0] === 'image') {
            return 'images';
          }
        };
        getTextFiles = function(fileRef, cb) {
          var reader;
          reader = new FileReader;
          reader.onload = function(e) {
            var result;
            result = {
              data: e.target.result,
              type: 'text'
            };
            return typeof cb === "function" ? cb({
              file: result
            }) : void 0;
          };
          return reader.readAsText(fileRef);
        };
        getImageFiles = function(fileRefs, cb) {
          var loadSingleImage, reader, result, totalCount;
          reader = new FileReader;
          totalCount = fileRefs.length;
          result = {
            data: [],
            type: 'images'
          };
          loadSingleImage = function() {
            return reader.readAsDataURL(fileRefs[result.data.length]);
          };
          reader.onload = function(e) {
            result.data.push(e.target.result);
            if (result.data.length === totalCount) {
              return typeof cb === "function" ? cb({
                file: result
              }) : void 0;
            } else {
              return loadSingleImage();
            }
          };
          return loadSingleImage();
        };
        Hammer(elm).on('doubletap', function() {
          return elm.find('.ppk-drop__input').click();
        });
        elm.find('.ppk-drop__input').on('change', function(e) {
          return scope.$apply(function() {
            return scope.handleUpload(e.target);
          });
        });
        scope.handleUpload = function(dt) {
          var type, _ref;
          if ((_ref = dt.files) != null ? _ref[0] : void 0) {
            type = getFileType(dt.files[0]);
            if (type === 'text') {
              return getTextFiles(dt.files[0], scope.onDrop);
            } else if (type === 'images') {
              return getImageFiles(dt.files, scope.onDrop);
            }
          }
        };
        scope.supportsUploadClickDelegation = !!$window.navigator.userAgent.match(/(iPod|iPhone|iPad)/) && $window.navigator.userAgent.match(/AppleWebKit/);
        boxEl.addEventListener('dragenter', onDragEnter, false);
        boxEl.addEventListener('dragleave', onDragLeave, false);
        boxEl.addEventListener('dragover', onDragOver, false);
        return boxEl.addEventListener('drop', onDrop, false);
      }
    };
  }
]);
