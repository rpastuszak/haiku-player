'use strict';
var App;

App = angular.module('app', ['templates', 'ngCookies', 'ngResource', 'app.controllers', 'ui.bootstrap', 'ui.bootstrap.tpls', 'pl.paprikka.haiku']);

App.config([
  '$routeProvider', '$locationProvider', '$modalProvider', '$tooltipProvider', function($routeProvider, $locationProvider, $modalProvider, $tooltipProvider) {
    $routeProvider.when('/404', {
      templateUrl: 'pages/404.html'
    }).when('/', {
      templateUrl: 'haiku/partials/views/import.html',
      controller: 'HaikuImportCtrl'
    }).when('/play/:roomID', {
      templateUrl: 'haiku/partials/views/play.html',
      controller: 'HaikuPlayCtrl'
    }).when('/view/:roomID', {
      templateUrl: 'haiku/partials/views/view.html',
      controller: 'HaikuViewCtrl'
    }).otherwise({
      redirectTo: '/404'
    });
    $locationProvider.html5Mode(false);
    $modalProvider.options = {
      modalOpenClass: 'disabled',
      backdrop: 'static',
      dialogClass: 'modal dialog-modal modal--default'
    };
    return $tooltipProvider.options({
      popupDelay: 600
    });
  }
]).run();
