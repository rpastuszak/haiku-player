'use strict'

# All dependencies should be loaded in the main app module
App = angular.module('app', [

  # ## Vendor modules / components
  'templates'
  'ngCookies'
  'ngResource'
  'ngRoute'
  'ngRoute'


  # ## Application components
  'app.controllers'

  'ui.bootstrap'
  'ui.bootstrap.tpls'

  'pl.paprikka.haiku'

])











App.config([

  '$routeProvider'
  '$locationProvider'
  '$modalProvider'
  '$tooltipProvider'

  ($routeProvider, $locationProvider, $modalProvider, $tooltipProvider) ->

    $routeProvider

      .when('/404', {templateUrl: 'pages/404.html'})

      .when('/', 
        templateUrl: 'haiku/partials/views/import.html', controller: 'HaikuImportCtrl'
      )

      .when('/test', {
        templateUrl: 'haiku/partials/views/test.html'
        controller: 'haiku.controllers.test'
      })

      .when('/play/:roomID', 
        templateUrl: 'haiku/partials/views/play.html', controller: 'HaikuPlayCtrl'
      )

      .when('/view/:roomID', 
        templateUrl: 'haiku/partials/views/view.html', controller: 'HaikuViewCtrl'
      )

      # Catch all / 404
      .otherwise({redirectTo: '/404'})

    # Without server side setup html5 pushState support must be disabled.
    $locationProvider.html5Mode off # Boo IE9, booo!



    $modalProvider.options =
      modalOpenClass: 'disabled'
      backdrop: 'static' # we user our custom overlay (and a flex-like effect as well)
      dialogClass: 'modal dialog-modal modal--default'

    $tooltipProvider.options 
      popupDelay: 600

]).run()


