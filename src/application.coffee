'use strict'

### Controllers ###


#  ## Main Application Controller
angular.module('app.controllers', [])
.controller('AppCtrl', [
  
  '$scope'
  '$location'
  '$rootScope'
  'Modal'


  ($scope, $location, $rootScope, Modal) ->


    $scope.application =
      initialized: yes


    # TODO: add generic error messages
    # $scope.onError = (data)-> 
    #   Modal.alert 'Something went wrong :(', 'An unexpected error has occured. You can still use your Haikµ locally, but some viewers might be out of sync. '
        
    # $rootScope.$on 'haiku:error', $scope.onError






    # TODO: enable watch unit testing on Karma

    # 1. Uses the url to determine if the selected
    # menu item should have the class `active`.
    # 2. Add $location ref to scope, so it could be watched
    $scope.$location = $location

    $scope.activeNavId = '/'
    setActiveNavId = (path) -> $scope.activeNavId = path or '/'

    $scope.$watch '$location.path()', setActiveNavId

    # Improved agular-seed version, runs even if no path was invoked yet.
    $scope.getClass = (id) ->
      if $scope.activeNavId?.substring(0, id.length) is id
        'active'
      else
        ''

]).run()
