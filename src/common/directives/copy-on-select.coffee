angular.module('app.common.directives.CopyOnSelect', []).directive('copyOnSelect', [
  ->
    restrict: 'A'
    link: (scope, elm, attr)->
      elm.on 'focus', (e) ->
        _.defer => this.select()
    
    

])