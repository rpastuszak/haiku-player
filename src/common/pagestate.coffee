angular.module('app.common.services.pageState', []).service( 'PageState', [

  '$window'

  ( $window ) ->
    class PageState
      enable : ->
        if location.hostname is 'localhost' or location.hostname.split('.')[0] is '192' then return # disable in dev environment
        $window.onbeforeunload = ->
          'Do you really want to leave and remove your haiku?'

      disable : ->
        $window.onbeforeunload = null

    new PageState

])
