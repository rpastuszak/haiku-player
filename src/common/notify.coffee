angular.module('app.common.services.Notify', []).factory 'Notify', -> 
  toastr.options.positionClass = 'toast-bottom-right'
  toastr.options.hideDuration = 2
  window.toastr