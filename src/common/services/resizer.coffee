angular.module('app.common.services.Resizer', []).service 'Resizer', [
  '$q'
  ( $q )->
    Resizer = ->
    Resizer::resize = (img, options) ->
      deferred = $q.defer()

      defaults = {
        maxSize: 1024
      }

      options = _.extend defaults, options


      canvas  = document.createElement 'canvas'
      context = canvas.getContext '2d'

      width   = img.width
      height  = img.height

      if width > height
        if width > options.maxSize
          height = height * options.maxSize / width
          width  = options.maxSize
      else
        if height > options.maxSize
          width   = width * options.maxSize / height
          height  = options.maxSize
    
      canvas.width    = width
      canvas.height   = height


      context.drawImage img, 0, 0, width, height
      console.log 'Resizer::resize, working...'
    
      resized = img: canvas.toDataURL 'image/jpeg', quality: .1
      # resized.img.replace 'image/jpeg', 'image/octet-stream'

      deferred.resolve resized
      deferred.promise

    new Resizer
]

  
  