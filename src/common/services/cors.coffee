angular.module('app.common.services.CORS', [])
.config(['$httpProvider', ($httpProvider)->
  # ## Cross-Origin Resource Sharing
  # Enables easy support for different request methods, otherwise
  # we would have to use JSONP requests
  console.log 'common.services.CORSService enabled.'
  delete $httpProvider.defaults.headers.common['X-Requested-With']
  delete $httpProvider.defaults.headers.common['Content-Type']
])
