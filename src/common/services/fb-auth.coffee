angular.module('app.common.services.FBAuth', []).service('FBAuth', [

  '$window'
  '$q'

  ( $window, $q )->
    config = {}
    FBAuth = ->
    userConfig = 
      appId: '374758215996599'
    config = userConfig

    FB.init appId: userConfig.appId


    FBAuth.login = ->
      deffered = $q.defer()
      if FB.getUserID()
        deffered.resolve FB.getAuthResponse()
      else
        FB.login (response) ->
          if response?.status is 'connected'
            deffered.resolve response
          else
            deffered.reject response
      deffered.promise
      
      

    FBAuth

])