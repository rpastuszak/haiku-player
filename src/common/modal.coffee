angular.module("app.common.services.Modal", ['ui.bootstrap', 'ui.bootstrap.tpls'])
.service('Modal', [
  '$modal'
  ($modal)->
    window.m = $modal

    methods =
      alert : (title, body, icon)->
        alertCtrl = ($scope, $modalInstance, data) ->
          $scope.data = data
          $scope.close = -> $modalInstance.dismiss 'close'
          $scope.getIconClass = ->
            if data.icon 
              'icon-' + data.icon 
            else 
              ''


        modalInstance = $modal.open
          templateUrl: 'common/partials/modals/alert.html'
          controller: alertCtrl
          resolve:
            data: -> 
              title: title or 'Message'
              body: body
              icon: icon

        modalInstance.result.then ->


    _.extend $modal, methods
])
.service('ModalDefaults', ->
  settings = 
    buttons:
      yesNo: [
        {label: 'No', result: no, cssClass: 'btn--default'}
        {label: 'Yes', result: yes, cssClass: 'btn--positive'}
      ]
)
