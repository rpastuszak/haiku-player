angular.module('pl.paprikka.haiku.directives.nav', [
  'pl.paprikka.haiku.services.slides'
]).directive('haikuNav', [

  '$rootScope'
  '$location'
  'Slides'
  'Modal'

  ( $rootScope, $location, Slides, Modal )->
    scope:
      categories:    '='
      visible:       '='
      control:       '&navControl'
      enableRemote:  '&onEnableRemote'
      share:         '&onShare'
    restrict:     'AE'
    replace:      yes
    templateUrl:  'haiku/partials/directives/nav.html'

    link: (scope, elm, attr) ->

      $rootScope.$on 'haiku:room:readyToShare', -> 
        scope.$apply -> scope.readyToShare = yes

      scope.clientRole = $rootScope.clientRole
      
      scope.goto = (slide) -> 
        $rootScope.$emit 'haiku:remote:goto', slide

      scope.isCurrentCategory = (cat) -> 
        Slides.isCurrentCategory cat, scope.categories
      
      scope.isCurrentSlide = (slide)-> 
        Slides.isCurrentSlide slide, scope.categories


      # TODO: move to Modal.prompt service
      closeCtrl = ($scope, $modalInstance) ->
        $scope.ok = -> 
          $modalInstance.close yes
        $scope.cancel = -> $modalInstance.dismiss 'cancel'
    
      scope.close = -> 
        modalInstance = Modal.open
          templateUrl: 'haiku/partials/modals/close.html'
          controller: closeCtrl

        modalInstance.result.then (result) ->
          if result
            $location.path '/'

])