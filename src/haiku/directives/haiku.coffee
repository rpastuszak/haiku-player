angular.module('pl.paprikka.directives.haiku', []).directive('haiku', [

  '$window'
  '$sce'
  'Hammer'
  'Slides'
  '$rootScope'

  ( $window, $sce, Hammer, Slides, $rootScope )->
    scope:
      categories: '='
      onUpdate: '&'
    templateUrl: 'haiku/partials/haiku.html'
    restrict: 'AE'
    link: ( scope, elm, attrs )->

      initSettings = (scope)->
        scope.categories.currentCategory   = 0
        scope.categories.currentSlide      = 0

        scope.isLastCategory    = no
        scope.isLastSlide       = no

        scope.isFirstCategory   = no
        scope.isFirstSlide      = no

      initSettings scope

      scope.$watch 'categories.length', (n,o) ->
        return unless n isnt o
        initSettings scope

      scope.updatePosition = ->
        console.log 'haiku::updatePosition'
        _.each scope.categories, (cat, catIndex)->
          if catIndex < scope.categories.currentCategory
            cat.status = 'prev'
          else if catIndex is scope.categories.currentCategory
            cat.status = 'current'
          else if catIndex > scope.categories.currentCategory
            cat.status = 'next'

          _.each cat.slides, (slide)->
            if slide.index < scope.categories.currentSlide
              slide.status = 'prev'
            else if slide.index is scope.categories.currentSlide
              slide.status = 'current'
            else if slide.index > scope.categories.currentSlide
              slide.status = 'next'

        currCat     = scope.categories.currentCategory
        currSlide   = scope.categories.currentSlide

        scope.isLastCategory  = if currCat is scope.categories.length - 1 then yes else no
        scope.isLastSlide     = if currSlide is scope.categories[currCat]?.slides?.length - 1 then yes else no
        scope.isFirstCategory = if currCat is 0 then yes else no
        scope.isFirstSlide    = if currSlide is 0 then yes else no

        scope.onUpdate? status: Slides.package scope.categories



      scope.prevCategory = ->
        unless scope.isFirstCategory
          scope.categories.currentCategory = scope.categories.currentCategory - 1
          scope.categories.currentSlide = 0

      scope.nextCategory = ->
        unless scope.isLastCategory
          scope.categories.currentCategory = scope.categories.currentCategory + 1
          scope.categories.currentSlide = 0

      scope.prevSlide = ->
        unless scope.isFirstSlide
          scope.categories.currentSlide = scope.categories.currentSlide - 1

      scope.nextSlide = ->
        unless scope.isLastSlide
          scope.categories.currentSlide = scope.categories.currentSlide + 1

      scope.$watch 'categories.currentCategory', scope.updatePosition
      scope.$watch 'categories.currentSlide', scope.updatePosition


      Hammer(elm).on 'swipeleft',  (e)->
        e.gesture.srcEvent.preventDefault();
        scope.$apply scope.nextCategory
        off

      Hammer(elm).on 'swiperight', (e)->
        e.gesture.srcEvent.preventDefault();
        scope.$apply scope.prevCategory
        off

      Hammer(elm).on 'swipeup',    (e)->
        e.gesture.srcEvent.preventDefault();
        scope.$apply scope.nextSlide
        off

      Hammer(elm).on 'swipedown',  (e)->
        e.gesture.srcEvent.preventDefault();
        scope.$apply scope.prevSlide
        off


      onKeyDown = (e) ->
        unless scope.$$phase then scope.$apply ->
          switch e.keyCode
            when 37 then scope.prevCategory()
            when 38 then scope.prevSlide()
            when 39 then scope.nextCategory()
            when 40 then scope.nextSlide()
        # e.preventDefault()
        # e.stopImmediatePropagation()
        # no


      onMouseWheel = (e) ->
        delta = e.originalEvent.wheelDelta
        treshold = 100
        scope.$apply ->
          if delta < -treshold
            scope.nextSlide()
          if delta > treshold
            scope.prevSlide()



      $($window).on 'keydown', onKeyDown
      $($window).on 'mousewheel', onMouseWheel

      $rootScope.$on 'haiku:remote:control', (e, data) ->

        scope.$apply ->
          if data.command is 'position'
            scope.goto
              index: data.params.currentSlide, categoryIndex: data.params.currentCategory

          # switch data.params?.direction
          #   when 'up'    then scope.prevSlide()
          #   when 'down'  then scope.nextSlide()
          #   when 'left'  then scope.prevCategory()
          #   when 'right' then scope.nextCategory()



      scope.$on 'haiku:goto', (slide) ->
        scope.goto slide

      scope.$on 'haiku:goto', (slide) ->
        scope.goto slide

      $rootScope.$on 'haiku:remote:goto', (e, slide)-> scope.goto slide

      scope.getCategoryClass = (category) ->
        'haiku__category--' + (category.status or 'prev')

      scope.getSlideClass = (slide) ->
        'haiku__slide--' + (slide.status or 'prev')

      scope.getSlideStyle = (slide)->
        'background' : slide.background or '#333'
        'background-size': 'cover'

      scope.isCurrentSlide = (slide) -> Slides.isCurrentSlide slide, categories

      scope.goto = (slide)->
        scope.categories.currentCategory = slide.categoryIndex
        scope.categories.currentSlide    = slide.index


      # TODO: extend theming support
      scope.getThemeClass = -> 'haiku--default'




     # TODO: move to a subdirective

])
