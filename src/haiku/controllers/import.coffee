angular.module('pl.paprikka.haiku.controllers.import', [
  'pl.paprikka.haiku.services.importer'
  'app.common.services.Notify'
]).controller('HaikuImportCtrl', [

  'Importer'
  '$location'
  '$rootScope'
  '$scope'
  'Remote'
  'Modal'
  'Notify'

  ( Importer, $location, $rootScope, $scope, Remote, Modal, Notify )->
    $rootScope.categories   = []
    $scope.files            = []
    $scope.state = 'idle'

    $scope.mdSupported = if navigator.userAgent.match /(iPad|iPhone|iPod)/g then no else yes

    Remote.connect()
    
    $scope.initConnection = (categories)->
      Remote.request categories

    $scope.onFileDropped = (data)->
      unless $scope.$$phase then $scope.$apply -> $scope.state = 'sending'
      _.defer -> $scope.$apply ->

        $rootScope.$on 'haiku:room:accepted', (e, data) ->
          $rootScope.clientRole = 'host'
          $location.path '/play/' + data.room
          $scope.$apply()

        Importer.getFromFiles(data).then (result) ->
          $rootScope.categories = result
        
        
          window.categories = $rootScope.categories
          if $rootScope.categories.length
            $scope.initConnection $rootScope.categories

          console.log 'Categories loaded: ', $scope.categories


    
  




])