HaikuPlayCtrl = angular.module('pl.paprikka.haiku.controllers.play', [

  # 'pl.paprikka.haiku.directives.nav'
  'app.common.services.Modal'
  'app.common.directives.CopyOnSelect'
]).controller('HaikuPlayCtrl', [

  '$location'
  '$routeParams'
  '$rootScope'
  '$scope'
  '$timeout'
  'Remote'
  'Slides'
  'Modal'
  'Notify'
  'PageState'

  ( $location, $routeParams, $rootScope, $scope, $timeout, Remote, Slides, Modal, Notify, PageState )->

    PageState.enable()

    showUI = ->
      $scope.UIReady = yes

    $timeout showUI, 2000


    $rootScope.$on 'haiku:remote:URLShared', ->
      Notify.info 'Invitations sent.'

    $rootScope.$on 'haiku:room:readyToShare', ->
      Notify.info 'Haiku is ready to share.'

    $rootScope.$on 'haiku:remote:URLSent', ->
      Notify.info 'Remote bookmark sent.'

    $rootScope.$on 'haiku:room:remoteJoined', ->
      console.log 'Haiku::Remote joined, sending current status...'
      $scope.updateStatus Slides.package $scope.categories
      Notify.info 'Remote connected.'

    $rootScope.$on 'haiku:room:guestJoined', ->
      Notify.info 'New guest has joined.'


    $scope.updateStatus = (data)->
      Remote.sendUpdates data, $routeParams.roomID









    # TODO: move to Modal.prompt service
    sendCtrl = ($scope, $modalInstance) ->
      $scope.result = {}
      $scope.currentEncodedURL = encodeURIComponent haiku.config.remoteURL + '/#/' + $routeParams.roomID

      onConnected = $rootScope.$on 'haiku:room:remoteJoined', ->
        $scope.cancel()
        onConnected()
        
      $scope.ok = (isOK)->
        if isOK
          $modalInstance.close $scope.result.email
      $scope.cancel = -> $modalInstance.dismiss 'cancel'



    $scope.sendRemoteURL = ->
      modalInstance = Modal.open
        templateUrl: 'haiku/partials/modals/send-remote-url.html'
        controller: sendCtrl


      modalInstance.result.then (email) ->
        Remote.sendRemoteURL $routeParams.roomID, email









    shareCtrl = ($scope, $modalInstance) ->
      $scope.newEmail = value: ''
      $scope.emails = []



      getCurrentURL = ->
        location.toString().replace(/#\/play\//i, "#/view/")
      $scope.currentURL = getCurrentURL()
      $scope.currentEncodedURL = encodeURIComponent $scope.currentURL

      $scope.add = (email) ->
        if email?.length
          $scope.emails.push email
          $scope.newEmail.value = ''


      $scope.remove = (email) ->
        $scope.emails = _.filter $scope.emails, (em)-> em isnt email


      $scope.ok = ->
        $modalInstance.close $scope.emails

      $scope.cancel = -> $modalInstance.dismiss 'cancel'



    $scope.shareURL = ->
      modalInstance = Modal.open
        templateUrl: 'haiku/partials/modals/share.html'
        controller: shareCtrl

      modalInstance.result.then (emails) ->
        console.log   emails
        Remote.shareURL $routeParams.roomID, emails














    $location.path('/') unless $rootScope.categories?.length
    $scope.navVisible = yes

])