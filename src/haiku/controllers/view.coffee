angular.module('pl.paprikka.haiku.controllers.view', [

  # 'pl.paprikka.haiku.directives.nav'

]).controller('HaikuViewCtrl', [

  '$http'
  '$location'
  '$routeParams'
  '$rootScope'
  '$scope'
  '$timeout'
  'Remote'

  ( $http, $location, $routeParams, $rootScope, $scope, $timeout, Remote )->

    $scope.status = 'loading'

    $rootScope.clientRole = 'guest'
    $scope.test = 'bar'

    $location.path('/') unless $routeParams.roomID

    Remote.join $routeParams.roomID

    $rootScope.$on 'haiku:room:joined', (scope, data) ->
      onDataLoaded = (res)->
        Remote.broadcastJoinedGuest data.room
        $rootScope.categories = res.categories
        $scope.status = 'ready'

        showUI = ->
          $scope.UIReady = yes

        $timeout showUI, 2000

      $http.get(data.categories).success onDataLoaded






    $scope.navVisible = yes

])
