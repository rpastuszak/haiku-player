angular.module('pl.paprikka.haiku.services.slides', []).factory('Slides',[ ->


    Slides = ->
    Slides.getCurrentCategory = (categories) ->
      _.find categories, status: 'current'
    
    Slides.isCurrentCategory = (cat) -> cat.status is 'current'
    
    Slides.isCurrentSlide = (slide, categories) ->
      slide.index is categories.currentSlide and slide.categoryIndex is categories.currentCategory    
    
    Slides.package = (categories) ->
      cleanedCategories = _.map categories, (cat)->
        slides : _.map cat.slides, (slide) ->
            index: slide.index      
            categoryIndex: slide.categoryIndex
      cleanedCategories =  JSON.parse angular.toJson(cleanedCategories)
      _.extend { categories: cleanedCategories }, _.pick categories, 'currentCategory', 'currentSlide' 
            

    Slides  
      
])