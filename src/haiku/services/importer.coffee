angular.module('pl.paprikka.haiku.services.importer', [
  'pl.paprikka.services.markdown'
  'app.common.services.Resizer'
  ]).factory('Importer', [

  'Markdown'
  'Resizer'
  '$q'

  ( Markdown, Resizer, $q )->

    defaultColors = [
      '#1abc9c'
      '#2ecc71'
      '#3498db'
      '#9b59b6'
      '#34495e'
      '#16a085'
      '#27ae60'
      '#2980b9'
      '#8e44ad'
      '#2c3e50'
      '#95a5a6'
      '#d35400'
      '#c0392b'
      '#7f8c8d'
    ]

    markdown = ""



    indexSlides = (categories) ->
      indexedCategories = _.cloneDeep categories
      _.each indexedCategories, (cat, catIndex)->
        _.each cat.slides, (slide, slideIndex)->
          slide.categoryIndex = catIndex
          slide.index = slideIndex
          slide
      indexedCategories

        

    markdownToSlides = (md) ->
      return [] unless md.length      

      slideBody       = Markdown.convert md
      categoryBodies  = slideBody.split('<hr>')

      _.each categoryBodies, (categoryBody) -> categoryBody = categoryBody.trim()

      newCategories = []

      _.each categoryBodies, (cat) ->
        slidesContents  = cat.replace(/<h1.*?>/gi, '__PAGE_BREAK__<h1>').split '__PAGE_BREAK__'
        newCategory     = slides: []

        _.each slidesContents, (sc) ->
          sc = sc.trim()
          if sc.length
            newSlide = {}
            regex = /<p><img(.*?)src="(.*?)" alt="background"><\/p>/gi

            parsedMarkdown = sc.replace regex, (whole, a, src, c) ->
              if src.indexOf('#') is 0
                newSlide.background = src 
              else
                newSlide.background = 'url(' + src + ')'
              ''
              
            newSlide.body = parsedMarkdown
            unless newSlide.background
              newSlide.background = defaultColors[Math.floor Math.random() * defaultColors.length]

            newCategory.slides.push newSlide

        newCategories.push newCategory
      newCategories
    
    
    
    Importer = ->
    Importer.get = -> 
      indexSlides markdownToSlides markdown
      
    
    Importer.getFromFiles = (files) ->
      deferred = $q.defer()

      if files.type is 'text'
        result = Importer.getFromMarkdown files.data
        deferred.resolve result
      else if files.type is 'images'
        Importer.getFromImages files.data, deferred
      else
        []
      # TODO: move haikuDrop logic here?
      deferred.promise
      
    Importer.getFromMarkdown = (markdown) ->
      indexSlides markdownToSlides markdown
    
    Importer.getFromImages = (images, deferred) ->
      categories = [ {slides: []} ]
      _.each images, (img) ->

        imgEl = new Image
        imgEl.onload = ->
          Resizer.resize(imgEl).then (resized) ->
            slide =
              background: 'url(' + resized.img + ')'
            categories[0].slides.push slide
            if categories[0].slides.length is images.length
              
              deferred.resolve indexSlides categories

        imgEl.src = img


      
     
    

    Importer  
      
])