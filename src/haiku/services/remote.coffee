angular.module('pl.paprikka.haiku.services.remote', ['app.common.webSockets']).service('Remote', [
  'WebSockets'
  '$rootScope'
  ( WebSockets, $rootScope) ->
    console.log 'Remote::init'
    SOCKET_LOCATION = haiku.config.hubURL
    socket = WebSockets.connect SOCKET_LOCATION

    socket.on 'connect', ->
      console.log 'haiku::Remote::connected'
      $rootScope.$apply()

      socket.on 'room:readyToShare', (data)->
        $rootScope.$emit 'haiku:room:readyToShare', data

      socket.on 'error', (data)->
        console.error 'Socket error', data
        $rootScope.$emit 'haiku:error', data

      socket.on 'remote', (data) ->
        $rootScope.$emit 'haiku:remote:control', data

      socket.on 'room:remoteJoined', (data)->
        $rootScope.$emit 'haiku:room:remoteJoined', data

      socket.on 'room:guestJoined', (data)->
        $rootScope.$emit 'haiku:room:guestJoined', data

      socket.on 'room:accepted', (data)->
        $rootScope.$emit 'haiku:room:accepted', data

      socket.on 'room:joined', (data)->
        $rootScope.$emit 'haiku:room:joined', data

      socket.on 'remote:URLSent', (data)->
        $rootScope.$emit 'haiku:remote:URLSent', data

      socket.on 'remote:URLShared', (data)->
        $rootScope.$emit 'haiku:remote:URLShared', data


    clearListeners = ->
      listeners = $rootScope.$$listeners
      _.forIn listeners, (value, key) ->
        if key.split(':')[0] is 'haiku'
          $rootScope.$$listeners[key] = null
      
      
      
      

    Remote =
      join : (room) ->
        socket.emit 'room:join', { room: room }
      leave : (room, cb) ->
        socket.emit 'room:leave', { room: room }
      request : (categories) ->
        socket.emit 'room:request', { categories: categories }
      sendRemoteURL : (room, to) ->
        socket.emit 'remote:sendURL', { room, to }
      shareURL : (room, to) ->
        socket.emit 'remote:shareURL', { room, to }
      broadcastJoinedGuest: (room) ->
        socket.emit 'remote:guestJoined', { room }

      connect: ->
        clearListeners()



      sendUpdates: (data, room)->
        console.log 'haiku::update', data, room, socket
        socket.emit 'remote:update', {data, room}
])
