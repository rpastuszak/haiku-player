angular.module('pl.paprikka.directives.drop', [
])
.directive('ppkDrop', [

  '$window'
  'Modal'

  ($window, Modal)->
    restrict:     'AE'
    templateUrl:  'drop/drop.html'
    replace:      yes
    scope:
      onDrop: '&'
      files:  '='
    link: (scope, elm, attrs)->
      boxEl = elm.find('.ppk-drop__box')[0]

      scope.isFileOver = no

      onDragEnter = (e) ->
        # e.stopPropagation()
        # e.preventDefault()
        scope.$apply ->
          scope.isFileOver = yes

      onDragLeave = (e) ->
        # e.stopPropagation()
        # e.preventDefault()
        scope.$apply ->
          scope.isFileOver = no

      onDragOver = (e) ->
        e.preventDefault()


      messages =
        unsupportedType:
          title: 'Unsupported file type'
          body: """
            Use Markdown (*.md, *.txt) or images to create a Haikµ. <br>
            Like to see a different format / feature here? <a target="_blank" href="mailto:gethaiku@gmail.com">Let us know</a>!
          """
          icon: 'upload'

      onDrop = (e) ->
        e.stopPropagation()
        e.preventDefault()
        dt = e.dataTransfer
        scope.$apply ->
          scope.files = dt.files
          scope.isFileOver = no

        scope.handleUpload dt


      # MIME type helper functions
      getFileType = ( fileDesc ) ->
        regex = /(\.[^.]+)$/i
        ext = fileDesc.name.match(regex)?[0]
        if fileDesc.type is ''
          switch ext
            when '.md' then 'text'
            when '.txt' then 'text'
        else if fileDesc.type.split('/')[0] is 'image'
          'images'
        else 'text'



      getTextFiles = (fileRef, cb) ->
        reader = new FileReader
        reader.onload = (e)->
          result =
            data: e.target.result
            type: 'text'
          cb? file:result

        reader.readAsText fileRef


      getImageFiles = (fileRefs, cb) ->
        reader      = new FileReader
        totalCount  = fileRefs.length

        result =
          data : []
          type : 'images'

        loadSingleImage = ->
          reader.readAsDataURL fileRefs[result.data.length]

        reader.onload = (e) ->
          result.data.push e.target.result
          if result.data.length is totalCount
            cb? file: result
          else
            loadSingleImage()
        loadSingleImage()




      Hammer(elm).on 'doubletap', -> elm.find('.ppk-drop__input').click()
      elm.find('.ppk-drop__input').on 'change', (e) ->
        scope.$apply -> scope.handleUpload e.target


      scope.handleUpload = (dt)->
        debugger
        if dt.files?[0]
          type = getFileType dt.files[0]
          if type is 'text'
            getTextFiles dt.files[0], scope.onDrop
          else if type is 'images'
            getImageFiles dt.files, scope.onDrop
          else
            m = messages.unsupportedType
            Modal.alert m.title, m.body, m.icon

      scope.supportsUploadClickDelegation = !!$window.navigator.userAgent.match(/(iPod|iPhone|iPad)/) && $window.navigator.userAgent.match(/AppleWebKit/)



      boxEl.addEventListener 'dragenter', onDragEnter, off
      boxEl.addEventListener 'dragleave', onDragLeave, off
      boxEl.addEventListener 'dragover', onDragOver, off
      boxEl.addEventListener 'drop', onDrop, off



])
